// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownMadnessGameMode.h"
#include "TopDownMadnessPlayerController.h"
#include "TopDownMadness/Character/TopDownMadnessCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopDownMadnessGameMode::ATopDownMadnessGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDownMadnessPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
