// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TopDownMadnessGameMode.generated.h"

UCLASS(minimalapi)
class ATopDownMadnessGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATopDownMadnessGameMode();
};



