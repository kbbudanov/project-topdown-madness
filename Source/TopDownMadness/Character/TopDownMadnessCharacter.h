// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TopDownMadness/FuncLibrary/Types.h"
#include "TopDownMadnessCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownMadnessCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATopDownMadnessCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComp) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UDecalComponent* CursorToWorld;
	

public:

	// ������ ��������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SpintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;

	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputMouseWheel(float MouseWheelValue);
	

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Movement")
		float SpeedCurrent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SpeedMax = MovementInfo.SprintRunSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SpeedDeltaPlus= 80.f; 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SpeedDeltaMinus = 50.f;
	
	FTimerHandle SprintingStartTimer; //������ �������� �� ������ ������� ������������ void Sprinting(), � ��� �������� ������� � ������ ��������
	FTimerHandle SprintingStopTimer; //������  �������� �� ������ ������� ��������� ������� void StopSprinting(), � ��� ������������ �������� � ������������ �������
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
		float StaminaCurrent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float StaminaMax=250.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float StaminaDeltaMinus = 10.f; // �� ��� �������� �������� ������� � ������ ����� ������� SprintingStartTimer
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float StaminaDeltaPlus = 10.f; // �� ��� �������� ������������� ������� � ������ ����� ������� SprintingStopTimer
	

	//bool bCanSprint = false;

	//��� ������
	FTimerHandle CameraZoomTimer;
	bool CameraZoomActive = true; // ����, ������� ���������������� ���� �� ������� � ����������� (����������)) ������� CameraMouseWheelZooming
	bool CameraZoomOut; // ����, ���������� � ������� CameraMouseWheelZooming ����������� ����
	float MouseWheel = 0.0f;
	float CameraTargetZoomDistance = 0.0f; 
	float CameraCurrentZoomDistance = 0.0f;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float CameraZoomSpeed = 500.0f;
	


	//�������, ����������� MovementInput 
	UFUNCTION()
		void MovementTick(float DeltaTime);
	UFUNCTION()
		void MovementModeChangeToSprint();
	UFUNCTION()
		void MovementModeChangeToSprintStop();
	UFUNCTION()
		void MovementModeChangeToWalk();
	UFUNCTION()
		void MovementModeChangeToWalkStop();
	UFUNCTION()
		void MovementModeChangeToAiming();
	UFUNCTION()
		void MovementModeChangeToAimingStop();
	

	UFUNCTION()
		void CameraMouseWheelZooming();

	UFUNCTION(BlueprintCallable)
		void CharcterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	UFUNCTION()
		bool CanSprint(); //����� ��������� �������� �������� � ������� ������, ������� ��������� ����� �� ���������
	UFUNCTION()
		void Sprinting(); //�������, ���������� �������� SprintingStartTimer, � ��� �������� ������� � ������ ��������
	UFUNCTION()
		void StopSprinting();//�������, ���������� �������� SprintingStopTimer, � ��� ������������ �������� � ������������ �������

	
};

