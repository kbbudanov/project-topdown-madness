// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownMadnessCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathlibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"

ATopDownMadnessCharacter::ATopDownMadnessCharacter()
{
	StaminaCurrent = StaminaMax;
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	
}

void ATopDownMadnessCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
	
	
}

void ATopDownMadnessCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownMadnessCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownMadnessCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("MovementModeChangeToSprint"), IE_Pressed, this, &ATopDownMadnessCharacter::MovementModeChangeToSprint);
	NewInputComponent->BindAction(TEXT("MovementModeChangeToSprint"), IE_Released, this, &ATopDownMadnessCharacter::MovementModeChangeToSprintStop);
	NewInputComponent->BindAction(TEXT("MovementModeChangeToWalk"), IE_Pressed, this, &ATopDownMadnessCharacter::MovementModeChangeToWalk);
	NewInputComponent->BindAction(TEXT("MovementModeChangeToWalk"), IE_Released, this, &ATopDownMadnessCharacter::MovementModeChangeToWalkStop);
	NewInputComponent->BindAction(TEXT("MovementModeChangeToAiming"), IE_Pressed, this, &ATopDownMadnessCharacter::MovementModeChangeToAiming);
	NewInputComponent->BindAction(TEXT("MovementModeChangeToAiming"), IE_Released, this, &ATopDownMadnessCharacter::MovementModeChangeToAimingStop);

	NewInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATopDownMadnessCharacter::InputMouseWheel);

}

void ATopDownMadnessCharacter::InputAxisX(float Value)
{
	AxisX = Value;

}

void ATopDownMadnessCharacter::InputAxisY(float Value)
{
	AxisY = Value;

}

void ATopDownMadnessCharacter::InputMouseWheel(float MouseWheelValue)
{
	if (CameraZoomActive) //���������, � ����� ��� ���� ������� �������
	{
		MouseWheel = MouseWheelValue;
		//���������� ����������� ������� 
		if (MouseWheel < 0) 
		{

			CameraCurrentZoomDistance = CameraBoom->TargetArmLength;
			CameraTargetZoomDistance = CameraCurrentZoomDistance - 300.0f;
			if (CameraTargetZoomDistance >= 600.f) 
			{
				CameraZoomOut = false;
				GetWorldTimerManager().SetTimer(CameraZoomTimer, this, &ATopDownMadnessCharacter::CameraMouseWheelZooming, 0.01f, true, 0);
			}

		}
		else if (MouseWheel > 0)
		{
			CameraCurrentZoomDistance = CameraBoom->TargetArmLength;
			CameraTargetZoomDistance = CameraCurrentZoomDistance + 300.0f;
			if (CameraTargetZoomDistance <= 1500.f)
			{
				CameraZoomOut = true;
				GetWorldTimerManager().SetTimer(CameraZoomTimer, this, &ATopDownMadnessCharacter::CameraMouseWheelZooming, 0.01f, true, 0);
			}
		}
	}
	
}

void ATopDownMadnessCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(),0); ////��������! ����� �������� ���������� 0 ������! (�� 1��, ���� ��-�����������))

	if
		(myController)
	{	
		FHitResult ResultHit;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		
		//FindLookAtRotation - (������, ����).����������
		float ActorsYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, ActorsYaw, 0.0f)));
			
	}
	CharcterUpdate();
}

void ATopDownMadnessCharacter::MovementModeChangeToSprint()
{

	GetWorldTimerManager().SetTimer(SprintingStartTimer, this, &ATopDownMadnessCharacter::Sprinting, 0.5f, true, 0);
}

void ATopDownMadnessCharacter::MovementModeChangeToSprintStop()
{
	GetWorldTimerManager().ClearTimer(SprintingStartTimer);
	GetWorldTimerManager().SetTimer(SprintingStopTimer, this, &ATopDownMadnessCharacter::StopSprinting, 0.5f, true, 0);
	
}

void ATopDownMadnessCharacter::MovementModeChangeToWalk()
{
	UE_LOG(LogTemp, Warning, TEXT("WalkEnabled = true;"));
	WalkEnabled = true;
	ChangeMovementState();
}

void ATopDownMadnessCharacter::MovementModeChangeToWalkStop()
{
	UE_LOG(LogTemp, Warning, TEXT("WalkEnabled = false;"));
	WalkEnabled = false;
	ChangeMovementState();
}

void ATopDownMadnessCharacter::MovementModeChangeToAiming()
{
	UE_LOG(LogTemp, Warning, TEXT("AimEnabled = true;"));
	AimEnabled = true;
	ChangeMovementState();
}

void ATopDownMadnessCharacter::MovementModeChangeToAimingStop()
{
	AimEnabled = false;
	UE_LOG(LogTemp, Warning, TEXT("AimEnabled = false;"));
	ChangeMovementState();
}

bool ATopDownMadnessCharacter::CanSprint()
{
	//��� �� ���������� ������� ����������� ������� � ����������� ��������. ������ ����������� �������� ������� �� �������� ���������
	//��� ����, ��� �� �������� ����� �������� ��� ����������� ���������
	FVector ActorLookDirection = GetActorForwardVector();
	FVector ActorVelocity = GetVelocity();
	FVector ActorMovementDirection;

	if (ActorVelocity.Size() != 0){
		ActorMovementDirection = ActorVelocity / ActorVelocity.Size();
	}
	else{
		ActorMovementDirection = FVector{ 0,0,0 };
	}

	//����� ����� ���� �� ������� � ����� - �������� ���������� �������� � ���� ��� ��������� - �� ��������� ������
	//�� � �����, ��� ����� ����� ������ ��� ������, ���� �� ������ ��������� � ��������� ������� �� ����������� �������

	float xDifference = FMath::Abs(ActorLookDirection.X - ActorMovementDirection.X);
	float yDifference = FMath::Abs(ActorLookDirection.Y - ActorMovementDirection.Y);
	
	return ((xDifference<0.15) && (yDifference < 0.15));
}

void ATopDownMadnessCharacter::CharcterUpdate()
{
	float ResSpeed = 400.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeed;
		
		break; 
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATopDownMadnessCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SpintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;  /// � ��� ���� ���� ����� ������� �����-�� ������� ����� ��� ���
		
	}
	else
	{
		if (SpintRunEnabled) // ������� ���, ��� �� �� ����� ������� ���������� ������������ � �������� ����� ������ ���������
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::SprintRun_State;
			UE_LOG(LogTemp, Warning, TEXT("SprintRun_State"));
		}
		else if (WalkEnabled && !AimEnabled) // ���� if (WalkEnabled && !SpintRunEnabled && !AimEnabled)
		{
			MovementState = EMovementState::Walk_State;
			UE_LOG(LogTemp, Warning, TEXT("Walk_State"));
		}
		else if (!WalkEnabled && AimEnabled) // ���� if (WalkEnabled && !SpintRunEnabled && !AimEnabled)
		{
			MovementState = EMovementState::Aim_State;
			UE_LOG(LogTemp, Warning, TEXT("Aim_State"));
		}
		else if (WalkEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
			UE_LOG(LogTemp, Warning, TEXT("AimWalk_State"));
		}

	}
	CharcterUpdate();
}

void ATopDownMadnessCharacter::Sprinting()
{
	if (StaminaCurrent > 0)
	{
		if (CanSprint())
		{
			if (SpintRunEnabled != true) //���� �� ��������, �� �����!
			{
				GetWorldTimerManager().ClearTimer(SprintingStopTimer);
				SpintRunEnabled = true;
				UE_LOG(LogTemp, Warning, TEXT("SpintRunEnabled = true"));
				ChangeMovementState();
				SpeedCurrent = MovementInfo.RunSpeed;
			}

			if (SpeedCurrent > (SpeedMax - SpeedDeltaPlus))
			{
				SpeedCurrent = SpeedMax;
				GetCharacterMovement()->MaxWalkSpeed = SpeedCurrent;
			}
			else if (SpeedCurrent < SpeedMax)
			{
				SpeedCurrent += SpeedDeltaPlus;
				GetCharacterMovement()->MaxWalkSpeed = SpeedCurrent;
			}
			
			
			StaminaCurrent -= StaminaDeltaMinus;
			UE_LOG(LogTemp, Warning, TEXT("StaminaCurrent = %f"), StaminaCurrent);
			UE_LOG(LogTemp, Warning, TEXT("SpeedCurrent = %f"), GetCharacterMovement()->MaxWalkSpeed);
		}
		else
		{
			MovementModeChangeToSprintStop();
		}

	}
	else
	{
		MovementModeChangeToSprintStop();
	}
}

void ATopDownMadnessCharacter::StopSprinting()
{
	if (SpintRunEnabled != false) //���� ��������, �� �� �����!
	{
		SpintRunEnabled = false;
		SpeedCurrent = MovementInfo.RunSpeed;
		UE_LOG(LogTemp, Warning, TEXT("SpintRunEnabled = false"));
		ChangeMovementState();
	}
	if (StaminaCurrent < StaminaMax-StaminaDeltaPlus)
	{
		StaminaCurrent += StaminaDeltaPlus;
		UE_LOG(LogTemp, Warning, TEXT("StaminaCurrent = %f"), StaminaCurrent);
	}
	else
	{
		StaminaCurrent = StaminaMax;
		GetWorldTimerManager().ClearTimer(SprintingStopTimer);
	}
}


void ATopDownMadnessCharacter::CameraMouseWheelZooming()
{
	UE_LOG(LogTemp, Warning, TEXT("Zoom CameraMouseWheelZooming"));
	float DeltaTime = UGameplayStatics::GetWorldDeltaSeconds(GetWorld());
	CameraCurrentZoomDistance = FMath::FInterpConstantTo(CameraCurrentZoomDistance, CameraTargetZoomDistance, DeltaTime, CameraZoomSpeed);
	CameraBoom->TargetArmLength = CameraCurrentZoomDistance;

	//���������� ������� �������, ��� FInterpConstantTo ��������� ��������� � ����� ����� ������������ ��������
	if ((CameraZoomOut && CameraTargetZoomDistance <= CameraCurrentZoomDistance) || (!CameraZoomOut && CameraTargetZoomDistance >= CameraCurrentZoomDistance))
	{
		UE_LOG(LogTemp, Warning, TEXT("Zoom Done"));
		GetWorldTimerManager().ClearTimer(CameraZoomTimer);
		CameraZoomActive = true;
	}
	
}

