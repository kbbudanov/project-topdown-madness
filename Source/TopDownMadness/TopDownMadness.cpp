// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownMadness.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TopDownMadness, "TopDownMadness" );

DEFINE_LOG_CATEGORY(LogTopDownMadness)
 